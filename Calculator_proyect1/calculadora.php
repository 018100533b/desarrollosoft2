<?php
    class Calculadora{
        // atributos
        public $nro1;
        public $nro2;
        // Metodos u operaciones
        public function Sumar()
        {
            return $this->nro1 + $this->nro2;
        }

        public function Restar()
        {
            return $this->nro1 - $this->nro2;
        }
        public function multiplica()
        {
            return $this->nro1 * $this->nro2;
        }
        public function Dividir(){
            return $this->nro1 / $this->nro2;
        }
        private function Fact($nro)
        {
            if($nro == 0)
                return 1;
            else
                return $nro * $this->Fact($nro - 1);
        }
        public function Factorial()
        {
            return $this->Fact($this->nro1);
        }
        // Potencia, Seno, Tangente.
        public function Potencita($nro1, $nro2)
        {
            if($nro2 == 0)
            {
                return 1;
            }
            else{
                return $nro1 * pow($nro1 , $nro2 - 1);
            } 
        }
        public function Seno($nro1)
        {
            return(sin(deg2rad($nro1)));
        }
        public function Tangente($nro)
        {
            return(tan(($nro*pi()/180)));
        }
        public function obtenerPorcentaje($nro1, $nro2) {
            $porcentaje = round($nro1 * 100) / $nro2; 
            return $porcentaje;
        }
        public function RaizCuadra($nro1){
            $nro1=sqrt($nro1);
            return $nro1;
        }
    }
    
    ?>