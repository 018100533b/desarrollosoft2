<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/bootstrap-theme.min.css" />
    <link rel="stylesheet" href="Estilos.css" />
    <script src="js/jquery-1.12.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <title>Procesar PHP dentro del mismo formulario</title>
</head>

<body>
    <div class="container" id="tablita-in">
            <form action="#" method="POST">
                <table class="table table-bordered table-striped" id="tabla">
                    <tr>
                        <legend id="ti-1">Calculadora</legend>
                        <p style="color:black; font-size:large"> Nro1: <input type="text" name="txtNro1" /></p>
                        <p style="color:black; font-size:large"> Nro2: <input type="text" name="txtNro2" /></p>
                    </tr>
                    <tr id="FilaBtn1">
                        <td><input type="submit" class="btn btn-outline-secondary" name="btnSumar" value="+" /></td>
                        <td><input type="submit" class="btn btn-outline-secondary" name="btnRestar" value="-" /></td>
                        <td><input type="submit" class="btn btn-outline-secondary" name="btnMulti" value="*" /></td>
                    </tr>
                    <tr id="FilaBtn2">
                        <td><input type="submit" class="btn btn-outline-secondary" name="btnDividir" value="/" /></td>
                        <td><input type="submit" class="btn btn-outline-secondary" name="btnFactorial" value="Fact" /></td>
                        <td><input type="submit" class="btn btn-outline-secondary" name="btnPotencia" value="Poten" /></td>
                    </tr>
                    <tr id="FilaBtn3">
                        <td><input type="submit" class="btn btn-outline-secondary" name="btnSeno" value="Sen" /></td>
                        <td><input type="submit" class="btn btn-outline-secondary" name="btnTangente" value="Tange" /></td>
                        <td><input type="submit" class="btn btn-outline-secondary" name="btnPorcentaje" value="%" /></td>
                    </tr>
                    <tr id="FilaBtn3">
                        <td><input type="submit" class="btn btn-outline-secondary" name="btnRaiz" value="Raiz" /></td>
                        <td><input type="submit" class="btn btn-outline-secondary" name="btn-N-esima" value="RaizN" /></td>
                        <td><input type="submit" class="btn btn-outline-secondary" name="btnInversa" value="Inversa" /></td>
                    </tr>
                </table>
            </form>
        <?php
        if (isset($_POST)) {
            // Llamar a la clase Calculadora
            include("calculadora.php");
            $calculo = new Calculadora;
            $nro1 = $_POST['txtNro1'];
            $nro2 = $_POST['txtNro2'];
            $calculo->nro1 = $nro1;
            $calculo->nro2 = $nro2;

            if (isset($_POST['btnSumar'])) {
                $suma = $calculo->Sumar();
                echo "La suma de los numeros es: ", $suma;
            } else if (isset($_POST['btnRestar'])) {
                $Restar = $calculo->Restar();
                echo "La resta de los numeros es: ", $Restar;
            } else if (isset($_POST['btnDividir'])) {
                $Dividir = $calculo->Dividir();
                echo "La resta de los numeros es: ", $Dividir;
            } else if (isset($_POST['btnMulti'])) {
                $Multiplicacion = $calculo->multiplica();
                echo "La multiplicación de los numeros es: ", $Multiplicacion;
            } else  if (isset($_POST['btnFactorial'])) {
                $fact = $calculo->Factorial();
                echo "La resta de los numeros es: ", $fact;
            //Terminar
             } else  if (isset($_POST['btnPotencia'])) {
                $Poten = $calculo->Potencita($nro1, $nro2);
                echo "La potencia de los numeros es: ", $Poten;
            }else  if (isset($_POST['btnSeno'])) {
                $Sen = $calculo->Seno($nro1);
                echo "El seno de: ",$nro1, " es: ", $Sen;
            }
            else  if (isset($_POST['btnTangente'])) {
                $Tange = $calculo->Tangente($nro1);
                echo "La tangente de: ",$nro1," es: ", $Tange;
            }
            else  if (isset($_POST['btnPorcentaje'])) {
                $Porcentaje = $calculo->obtenerPorcentaje($nro1, $nro2);
                echo "El porcentaje de: ",$nro1, "es: ",$Porcentaje;
            }
            else  if (isset($_POST['btnRaiz'])) {
                $raiz = $calculo->RaizCuadra($nro1);
                echo "La raíz cuadrada de: ",$nro1, " es: ", $raiz;
            }
        }
        ?>
    </div>
</body>
</html>